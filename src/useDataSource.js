import { useState, useRef } from 'react';
import cloneDeep from 'clone-deep';

export default function useDataSource(initValue) {
    const [data, setData] = useState(initValue);
    const dataRef = useRef(data);

    const [tempData, setTempData] = useState(null);
    const tempDataRef = useRef(tempData);

    const finalDataRef = useRef(null);

    const processPath = (path) => {
        // path could be an array, it could be a string with dots, and array items could be strings with dots
        const newPath = [];
        if (Array.isArray(path)) {
            path.forEach((item) => {
                if (typeof item === "string") {
                    const items = item.split(".");
                    for (const subItem of items) {
                        newPath.push(subItem);
                    }
                } else {
                    // we hope it's something we can deal with but take care of it later
                    newPath.push(item);
                }
            });
        } else if (typeof path === "string") {
            const items = path.split(".");
            for (const subItem of items) {
                newPath.push(subItem);
            }
        } else {
            newPath.push(path);
        }

        return newPath;
    }

    const storeItemHelper = (obj, pathArr, item) => {
        const key = pathArr.shift();
        // if it's numeric, then create an array if we're creating the object
        let isArray = false;
        if (key == parseInt(key, 10)) {
            isArray = true;
        }

        const useObj = obj || (isArray ? [] : {});
        if (pathArr.length === 0) {
            // end of the line!
            useObj[key] = item;
        } else {
            const newValue = storeItemHelper(useObj[key], pathArr, item);
            useObj[key] = newValue;
        }

        return useObj;
    }

    const storeItem = (path, item) => {
        // path could be an array, it could be a string with dots, and array items could be strings with dots
        const newPath = processPath(path);

        const newData = cloneDeep(tempDataRef.current || dataRef.current);

        const obj = storeItemHelper(newData, newPath, item);

        setTempData(obj);
        tempDataRef.current = obj;
        finalDataRef.current = obj;
    }

    const commitData = () => {
        setData(tempDataRef.current);
        dataRef.current = tempDataRef.current;
        setTempData(null);
        tempDataRef.current = null;
        finalDataRef.current = dataRef.current;
    }

    const rollbackData = () => {
        setTempData(null);
        tempDataRef.current = null;
        finalDataRef.current = dataRef.current;
    }

    const deleteItemHelper = (obj, pathArr) => {
        const key = pathArr.shift();
        // if it's numeric, then create an array if we're creating the object
        let isArray = false;
        if (key == parseInt(key, 10)) {
            isArray = true;
        }

        const useObj = obj || (isArray ? [] : {});
        if (pathArr.length === 0) {
            // end of the line!
            if (Array.isArray(useObj)) {
                useObj.splice(key, 1);
            } else {
                delete useObj[key];
            }
        } else {
            const newValue = deleteItemHelper(useObj[key], pathArr);
            useObj[key] = newValue;
        }

        return useObj;
    }

    const deleteItem = (path) => {
        const newPath = processPath(path);

        const newData = cloneDeep(tempDataRef.current || dataRef.current);

        const obj = deleteItemHelper(newData, newPath);

        setTempData(obj);
        tempDataRef.current = obj;
        finalDataRef.current = obj;
    }

    const getItemHelper = (obj, pathArr) => {
        const key = pathArr.shift();
        // if it's numeric, then create an array if we're creating the object
        let isArray = false;
        if (key == parseInt(key, 10)) {
            isArray = true;
        }

        const useObj = obj || (isArray ? [] : {});
        if (pathArr.length === 0) {
            return useObj[key];
        } else {
            return getItemHelper(useObj[key], pathArr);
        }
    }

    const processDataHelper = (obj, cb) => {
        const isArray = Array.isArray(obj);

        const resultData = [];
        if (isArray) {
            for (let i=0;i<obj.length;i++) {
                resultData.push(cb({
                    item: obj[i],
                    key: i,
                    isArray,
                }));
            }
        } else {
            for (const key of obj) {
                const value = obj[key];
                resultData.push(cb({
                    item: value,
                    key,
                    isArray,
                }));
            }
        }

        if (resultData.length === 0) {
            return isArray ? [] : {};
        }

        // all results should have the same isArray set
        let tempIsArray = null;
        for (let i=0;i<resultData.length;i++) {
            const result = resultData[i];
            if (tempIsArray === null) {
                tempIsArray = result.isArray;
            }

            if (tempIsArray !== result.isArray) {
                console.error(`processForSave: result set at ${i} has isArray set to ${result.isArray}, which does not match previous values`);
                return null;
            }
        }

        let finalResult = {};

        if (resultData[0].isArray) {
            finalResult = [];
        }

        for (const result of resultData) {
            finalResult[result.key] = result.item;
        }

        return finalResult;
    }

    const processData = (path, cb) => {
        const newPath = processPath(path);
        const obj = getItemHelper(tempDataRef.current || dataRef.current, newPath);

        return processDataHelper(obj, cb);
    }

    const storeExternalItem = (path, data, cb) => {
        const finalData = processDataHelper(data, cb);

        storeItem(path, finalData);
    }

    return {
        data: tempData || data,
        dataRef: finalDataRef,
        storeItem,
        commitData,
        rollbackData,
        deleteItem,
        modified: tempDataRef.current !== null,
        processData,
        storeExternalItem,
    };
}