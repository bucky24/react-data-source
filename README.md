## @bucky24/react-data-source

This is a project I built that is designed to help more easily store transactional data in a React app that is using Contexts. It does several things:

* Provides an easy way to update and delete data by path
* Allows easy committing and rollback of data changes

Are there already modules out there that do this? I don't know, I didn't look. If I ever find something that seems to do something similar, I will link it here.

## Usage

The module exports a single method, `useDataStore`.

### Inputs

The method takes in a single optional parameter, which will be the initial data for the data store.

### Outputs

The method outputs an object with the following properties:

| Key | Type | Value |
| -- | -- | -- |
| data | Object/Array | The current value of the data in the data store |
| dataRef | Reference | A React reference to the data in the data store |
| modified | Boolean | A flag that is true if there are uncommitted changes in the data store |
| storeItem | Function | A function that takes in a Path and an item and stores that item in the data store |
| storeExternalItem | Function | A function that takes in a Path, an item, and a callback, and performs an operation on every item in the given item, then stores it on the Path. See below for details |
| deleteItem | Function | A function that takes in a Path and removes that item from the data store|
| commitData | Function | A function that moves any temporary changes to the permanent data store |
| rollbackData | Function | A function that wipes away any temporary changes and reverts to the permanent data store |
| processData | Function | A function that takes in a Path and a callback and performs an operation on every item at that point in the Path. See below for details |

### Path

A Path can be the following:

* A dot-path (`"key.key2.key3"`)
* An array where each element can be...
    * A dot-path
    * Numeric
    * Anything else that can be used as a key in JavaScript

Note that in the case of a numeric key, the system will attempt to treat the parent as an array if it does not already exist. For example, the following key:

```
user.1.name
```

Will create the following object:

```
{
    "user": [
        {
            "name": <value>
        }
    ]
}
```

But only if the `user` key does not already exist. The system will respect any existing data structure.

### storeExternalItem

The `storeExternalItem` function is meant for transforming data from outside the app or from other places in the app for use in the data store, It takes in as the first parameter, a `Path`. The second parameter is the data to transform then store. The third parameter is a callback function that has a `ProcessObject` as a paramter and expects a `ProcessObject` as a return value.

```
storeExternalData(pathObject, dataObject, (processObject) => {
    return newProcessObject;
});
```

For the parameter to the callback, the `isArray` field will tell you if the source data structure was an array, and the `key` is the object key or array index that this particular item was referenced by in the source data structure.

Likewise, in the response to the callback, the `isArray` field will tell the system if it should generate an array or an object. It is perfectly acceptable to switch types, where the source data structure is an array and the output data structure is an object, or vice-versa. The only limitation is that all `isArray` values in the output must match.
### processData

The `processData` function is meant for transforming data in the data store for use in other parts of the program, or for sending externally. It takes in as the first parameter, a `Path`, and as the second parameter, a callback function that has a `ProcessObject` as a paramter and expects a `ProcessObject` as a return value.

```
const resultObj = processData(pathObject, (processObject) => {
    return newProcessObject;
})
```

This method's callback performs exactly the same as the one for `storeExternalData`, except with the results being stored on the data store, rather than returned.

### ProcessObject

The ProcessObject contains the following fields:

| Key | Type | Value |
| -- | -- | -- |
| isArray | Boolean | Flag that indicates if this value is related to an array
| key | Mixed | The key or index to set on the resulting data structure. For isArray = true, this should be a number |
| item | Any | The item to set on the resulting data structure |