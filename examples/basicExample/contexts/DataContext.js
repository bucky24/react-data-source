import React from 'react';
import { useDataSource  } from '@bucky24/react-data-source';

const DataContext = React.createContext({});
export default DataContext;

export function DataProvider({ children }) {
    const {
        data,
        storeItem,
        commitData,
        rollbackData,
        deleteItem,
        modified,
        processData,
        storeExternalItem
    } = useDataSource({ user: [] });

    const value = {
        data,
        setName: (name, index) => {
            storeItem(`user.${index}.name`, name);
            storeItem(["user", index, "active"], true);
        },
        commitData,
        rollbackData,
        removeName: (index) => {
            deleteItem(["user", index]);
        },
        modified,
        addName: (newName) => {
            const index = data.user.length;
            value.setName(newName, index);
        },
        getSaveData: () => {
            return processData('user', ({ item, key, isArray }) => {
                const newItem = {
                    ...item,
                };

                delete newItem.active;

                return {
                    item: newItem,
                    key,
                    isArray,
                }
            });
        },
        loadSaveData: (data) => {
            storeExternalItem("user", data, ({ item, key, isArray }) => {
                return {
                    item: {
                        name: item.name,
                        active: true,
                    },
                    key,
                    isArray,
                };
            });
        }
    };

    return (
        <DataContext.Provider value={value}>
            {children}
        </DataContext.Provider>
    );
}