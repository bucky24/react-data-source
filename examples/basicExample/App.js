import React, { useEffect, useContext, useState } from 'react';

import DataContext from './contexts/DataContext';

import styles from './styles.css';

export default function App() {
	const {
		setName,
		data,
		commitData,
		rollbackData,
		removeName,
		modified,
		addName,
		getSaveData,
		loadSaveData,
	} = useContext(DataContext); 
	const [tempName, setTempName] = useState('');
	const [saveData, setSaveData] = useState('');

	useEffect(() => {
		setName("bob", 0);
		setName("frank", 1);
		commitData();
		setName("jaames", 2);
		rollbackData();
		setName("james", 2);
		commitData();
	}, []);

	console.log(data);

	return (<div className={styles.appRoot}>
		Names!<br />
		{data.user.map((user, index) => {
			return <div key={`user_${index}`}>
				<span>{user.name}</span>
				<span
					style={{
						cursor: 'pointer',
						padding: 8,
					}}
					onClick={() => {
						removeName(index);
					}}
				>X</span>
			</div>;
		})}
		<div>
			<input
				type="text"
				value={tempName}
				onChange={(e) => {
					setTempName(e.target.value);
				}}
			/>
			<button onClick={() => {
				if (tempName.length > 0) {
					addName(tempName);
					setTempName('');
				}
			}}>Add</button>
		</div>
		{modified && (
			<button onClick={rollbackData}>
				Rollback changes
			</button>
		)}
		{modified && (
			<button onClick={commitData}>
				Commit
			</button>
		)}
		<br />
		<textarea rows={10} cols={20} value={saveData} onChange={(e) => {
			setSaveData(e.target.value);
		}} />
		<br />
		<button onClick={() => {
			setSaveData(JSON.stringify(getSaveData()));
		}}>
			Generate Save Data
		</button>
		<button onClick={() => {
			loadSaveData(JSON.parse(saveData));
		}}>
			Load Save Data
		</button>
	</div>);
}